{
  i18n.defaultLocale = "de_DE.UTF-8";
  time.timeZone = "Europe/Berlin";
  #console.keyMap = "de";
  # Configure keymap in X11
  services.xserver.layout = "de";
  # services.xserver.xkbOptions = "eurosign:e";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "de";
  };
}
