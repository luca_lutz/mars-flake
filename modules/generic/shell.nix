{ pkgs, ... }: {
  environment.shellAliases = {
    system-update = ''nixos-rebuild switch --flake gitlab:luca_lutz/mars-flake/$(curl -s https://gitlab.com/api/v4/projects/52757989/repository/commits/ | ${pkgs.jq}/bin/jq -r -M ".[0].id")#mars'';
  };
}
