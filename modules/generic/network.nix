{
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.
  networking.hostName = "mars";
  networking.firewall.enable = true;
  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
}

