# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
 # Allow proprietary software (such as the NVIDIA drivers).
  nixpkgs.config.allowUnfree = true;

  # Enable the Plasma 5 Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.displayManager.gdm.wayland = false;
  services.xserver.desktopManager.plasma5.enable = true;
  # Window Managers
  #services.xserver.windowManager.stumpwm.enable = true;
  #services.xserver.windowManager.ratpoison.enable = true;
  #services.xserver.windowManager.exwm.enable = true;

  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true; # so that gtk works properly
    extraPackages = with pkgs; [
      swaylock
      swayidle
      wl-clipboard
      wf-recorder
      mako # notification daemon
      grim
     #kanshi
      slurp
      alacritty # Alacritty is the default terminal in the config
      dmenu # Dmenu is the default in the config but i recommend wofi since its wayland native
    ];
    extraSessionCommands = ''
      export SDL_VIDEODRIVER=wayland
      export QT_QPA_PLATFORM=wayland
      export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
      export _JAVA_AWT_WM_NONREPARENTING=1
      export MOZ_ENABLE_WAYLAND=1
    '';
  };

  programs.waybar.enable = true;

  # QT
  qt.platformTheme = "qt5ct";

  hardware.opengl.driSupport32Bit = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  #hardware.pulseaudio.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };
  security.pam.services.swaylock = { };

  services.greetd = {
    enable = true;
    settings = {
      default_session = {
        command = "${pkgs.greetd.greetd}/bin/agreety --cmd sway";
      };
    };
  };

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Fonts
  fonts.packages = with pkgs; [
    #fira-code
    #fira
    #cooper-hewitt
    #ibm-plex
    #jetbrains-mono
    #iosevka
    # bitmap
    #spleen
    #fira-code-symbols
    #powerline-fonts
    nerdfonts
  ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # editors
    neovim
    # tools
    gparted
    # kdiff3
    remmina
    # picom
    dmenu
    tmux
    filezilla
    vlc
    #etcher
    # coreutils
    # binutils
    wget
    curl
    git
    # man
    #mkpasswd
    unzip
    killall
    python3
    # webdav
    davfs2
    autofs5
    fuse
    sshfs
    cadaver
    # browsers
    firefox
    chromium
    # media
    #spotify
    # nix
    nixpkgs-lint
    nixpkgs-fmt
    nixfmt
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  #programs.mtr.enable = true;
  #programs.gnupg.agent = {
  #  enable = true;
  #  enableSSHSupport = true;
  #};

  # Virtmanager settings
  #programs.dconf.enable = true;
  #services.qemuGuest.enable = true;
  #virtualisation.libvirtd = {
  #  enable = true;
  #  qemuOvmf = true;
  #  qemuRunAsRoot = true;
  #  onBoot = "ignore";
  #  onShutdown = "shutdown";
  #};

  # Docker
  #virtualisation.docker.enable = true;
  #virtualisation.docker.enableOnBoot = true;

  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = true;

}
