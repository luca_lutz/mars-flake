# /etc/nixos/flake.nix
{
  description = "mars-notebook";

  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-unstable";
    };
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs = { nixpkgs, nixpkgs-unstable, nixos-hardware, home-manager, ... }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
    };
  in {
    packages."${system}".install = import ./install.nix { inherit pkgs; };
    nixosConfigurations = {
      mars = nixpkgs.lib.nixosSystem {
        inherit system pkgs;
        modules = [
          ./hardware-configuration.nix
          ./modules
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.luca = import ./home;
          }
        ];
      };
    };
  };
}
