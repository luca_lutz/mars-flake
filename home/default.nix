{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "luca";
  home.homeDirectory = "/home/luca";

  home.stateVersion = "23.05";
  
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.packages = with pkgs; [
    killall
    alacritty # gpu accelerated terminal
    mako # notification system
    bemenu # launch menu
    wl-clipboard
  ];

  xdg.enable = true;

  programs.bash.enable = true;

  imports = [
    ./sway
    #./kubectl.nix
    #./go.nix
    #./nvim
    #./kdeconnect.nix
    #./gpg.nix
    #./protonmail.nix
    #./gtk.nix
  ];
}